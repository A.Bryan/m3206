#! /usr/bin/env bash

echo "[...] Checking internet connection [...]"

#Test de la connexion 3 requetes ping vers racine Google en background
ping -c 3 8.8.8.8 > /dev/null 

# Ping OK
if [ $? -eq 0 ]
	then
		echo "[...] Internet acces OK [...] "
	else
		echo "[/!\] Not connected to Internet [/!\]"
		echo "[/!\] Please check configuration [/!\]"
		
fi 
 


