#! /usr/bin/env bash

# Script de mise à jour système Ubuntu

$user=`whoami`


if [$user != root]
	then
		echo [/!\] Vous devez être super-utilisateur [/!\]

	else 
	echo [...] update database [...]
	apt-get update &	#En tache de fond

	echo [...] upgrade system [...]
	apt-get dist-upgrade --yes	#Gestion intelligente de la MaJ
fi
